import { useEffect, useState } from "react";
import useBreedList from "../hooks/useBreedList";
import { useQuery } from "@tanstack/react-query";
import fetchPets from "../services/fetchPets";
import { Link } from "react-router-dom";

const ANIMALS = ["cat", "dog", "bird"];

function Pets() {
  const [requestParams, setRequestParams] = useState({
    animal: "",
    location: "",
    breed: "",
  });
  const [animal, setAnimal] = useState("");
  const breeds = useBreedList(animal);

  const petsResult = useQuery(["pets", requestParams], fetchPets);

  if (petsResult.isError) {
    return <div>Errored!!</div>;
  }

  const pets = (petsResult.data && petsResult.data.pets) || [];

  return (
    <div>
      <h1>Adopt Me</h1>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          const formData = new FormData(e.target);
          const obj = {
            animal: formData.get("animal"),
            location: formData.get("location"),
            breed: formData.get("breed"),
          };
          setRequestParams(obj);
        }}
      >
        <div>
          <label htmlFor="input-location">Location</label>
          <input
            id="input-location"
            type="text"
            name="location"
            placeholder="Location"
          ></input>
        </div>
        <div>
          <label htmlFor="select-animal">Animal</label>
          <select
            id="select-animal"
            name="animal"
            value={animal}
            onChange={(e) => {
              setAnimal(e.target.value);
            }}
          >
            <option value=""></option>
            {ANIMALS.map((an) => (
              <option key={an} value={an}>
                {an}
              </option>
            ))}
          </select>
        </div>
        <div>
          <label htmlFor="select-breed">Breed</label>
          <select id="select-breed" name="breed">
            <option value=""></option>
            {breeds &&
              breeds[0].map((b) => (
                <option key={b} value={b}>
                  {b}
                </option>
              ))}
          </select>
        </div>
        <div>
          <button type="submit">Submit</button>
        </div>
      </form>
      <div>
        <h2>Pets list</h2>
        <ul>
          {pets.map((p) => (
            <li>
              <Link to={`/details/${p.id}`}>{p.name}</Link>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default Pets;

import { Component } from "react";

class ErrorBoundary extends Component {
  state = {
    hasError: false,
  };

  static getDerivedStatefromError() {
    return { hasError: true };
  }

  componentDidCatch(err, info) {
    console.error("Error in ErrorBoundary", err, info);
  }

  render() {
    if (this.state.hasError) {
      return <h2>An unexpected error occured.</h2>;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;

import { useQuery } from "@tanstack/react-query";
import { useNavigate, useParams } from "react-router-dom";
import fetchPet from "../services/fetchPet";
import { useContext, useState } from "react";
import Modal from "./Modal";
import { AdoptedPetContext } from "../contexts/AdoptedPetContext";

function PetDetails() {
  const { id } = useParams();

  const [showModal, toggleModal] = useState(false);

  const { adoptedPet } = useContext(AdoptedPetContext);
  const [_, setAdoptedPet] = adoptedPet;

  const result = useQuery(["petDetails", id], fetchPet);

  const navigate = useNavigate();

  if (result.isLoading) {
    return <div>Loading...</div>;
  }

  if (result.isError) {
    return <div>Errored!!</div>;
  }

  const pet = (result.data && result.data.pets[0]) || {};

  return (
    <div className="details">
      <div>
        <h1>{pet.name}</h1>
        <h2>{`${pet.animal} — ${pet.breed} — ${pet.city}, ${pet.state}`}</h2>
        <button onClick={() => toggleModal(true)}>Adopt {pet.name}</button>
        {showModal ? (
          <Modal>
            <div>
              <h1>Would you like to adopt {pet.name}?</h1>
              <div className="buttons">
                <button
                  onClick={() => {
                    setAdoptedPet(pet.id);
                    navigate("/pet-adopted");
                  }}
                >
                  Yes
                </button>
                <button onClick={() => toggleModal(false)}>No</button>
              </div>
            </div>
          </Modal>
        ) : null}
        <p>{pet.description}</p>
      </div>
    </div>
  );
}

export default PetDetails;

import { useContext } from "react";
import { AdoptedPetContext } from "../contexts/AdoptedPetContext";

function PetAdopted() {
  const { adoptedPet } = useContext(AdoptedPetContext);
  const [petAdopted] = adoptedPet;

  return (
    <div>
      <h3>Congratulations! You have adopted pet with id: {petAdopted}!</h3>
    </div>
  );
}

export default PetAdopted;

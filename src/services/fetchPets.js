import { PETS_BASE_URL } from "../constants";

async function fetchPets({ queryKey }) {
  const { animal, location, breed } = queryKey[1];

  const res = await fetch(
    `${PETS_BASE_URL}/pets?animal=${animal}&location=${location}&breed=${breed}`,
  );
  if (!res.ok) {
    throw new Error("Error in fetching pets");
  }

  return res.json();
}

export default fetchPets;

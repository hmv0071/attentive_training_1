import { PETS_BASE_URL } from "../constants";

async function fetchBreeds({ queryKey }) {
  const animal = queryKey[1];
  if (!animal) {
    return [];
  }

  const res = await fetch(`${PETS_BASE_URL}/breeds?animal=${animal}`);
  if (!res.ok) {
    throw new Error("Error fetching breeds");
  }

  return res.json();
}

export default fetchBreeds;

import { createContext, useState } from "react";

export const AdoptedPetContext = createContext();

function AdoptedPetContextProvider({ children }) {
  const adoptedPet = useState(null);

  return (
    <AdoptedPetContext.Provider value={{ adoptedPet }}>
      {children}
    </AdoptedPetContext.Provider>
  );
}

export default AdoptedPetContextProvider;

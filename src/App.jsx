import "./styles/style.css";

import Pets from "./components/Pets";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import PetDetails from "./components/PetDetails";
import ErrorBoundary from "./components/utils/ErrorBoundary";
import AdoptedPetContextProvider from "./contexts/AdoptedPetContext";
import PetAdopted from "./components/PetAdopted";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: Infinity,
      cacheTime: Infinity,
    },
  },
});

const App = () => {
  return (
    <ErrorBoundary>
      <BrowserRouter>
        <AdoptedPetContextProvider>
          <QueryClientProvider client={queryClient}>
            <Routes>
              <Route path="/details/:id" element={<PetDetails />} />
              <Route path="/pet-adopted" element={<PetAdopted />} />
              <Route path="/" element={<Pets />} />
            </Routes>
          </QueryClientProvider>
        </AdoptedPetContextProvider>
      </BrowserRouter>
    </ErrorBoundary>
  );
};

export default App;

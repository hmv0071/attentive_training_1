import { useQuery } from "@tanstack/react-query";
import fetchBreeds from "../services/fetchBreeds";

function useBreedList(animal) {
  const breedsResult = useQuery(["breed", animal], fetchBreeds);

  const breeds = (breedsResult.data && breedsResult.data.breeds) || [];

  return [breeds];
}

export default useBreedList;
